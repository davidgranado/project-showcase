# ProjectGallery.me

Project Description

<em>[TODO.md spec & Kanban Board](https://bit.ly/3fCwKfM)</em>

### Long Term Todo

- [ ] Allow users to charge for guaranteed response to DM  

### Bugs to hunt

- [ ] FIx bug where you are signed out after sitting for a bit  

### Todo

- [ ] Delete journal posts  
- [ ] Delete Posts  
- [ ] Sort projects gallery  
- [ ] Add project labels/categories  
- [ ] Filter on labels/categories  
- [ ] Clarify intention of project on home page  
- [ ] Figure out what content to present on main page  
- [ ] Determine shorter term premium offering  
- [ ] Add more links to journal posts like previous/next entry  
- [ ] Add images to journal posts  
- [ ] Implement search  
- [ ] Follow users and proejcts  
- [ ] Add bookmarking of posts  
- [ ] Add email notifications of updates on followed users/projects  
- [ ] Add email password recovery  
- [ ] Add email to users  

### Done

- [ ] Ensure only published journals are shown unless you are the owner  
- [ ] Add "Links" section  
- [ ] Sort project images  

